import Modal from './Modal'
import Header from './ModalComponents/ModalHeader'
import Footer from './ModalComponents/ModalFooter'
import Body from './ModalComponents/ModalBody'
import Close from './ModalComponents/ModalClose'
import Wrapper from './ModalComponents/ModalWrapper'

function ModalText({ modal, setModal }) {
  return modal ? (
    <Wrapper
      onClick={(event) => {
        if (event.target.classList.contains('modal__wrapper')) {
          setModal(!modal)
        }
      }}
    >
      <Modal>
        <Header>
          <Close
            onClick={() => {
              setModal(!modal)
            }}
          />
        </Header>

        <Body>
          <h2 className="modal__body-title">Add Product “NAME”</h2>
          <p className="modal__body-text">Description for you product</p>
        </Body>

        <Footer
          firstText="ADD TO FAVORITE"
          // firstClick={handleFirstClick}
        />
      </Modal>
    </Wrapper>
  ) : null
}

export default ModalText
