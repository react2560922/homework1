import { useState } from 'react'
import Button from './components/Button'

import ModalImage from './components/ModalImage'

import ModalText from './components/ModalText'
import './index.scss'

function App() {
  const [modal, setModal] = useState(false)
  const [modal2, setModal2] = useState(false)

  const openModal = () => {
    setModal(true)
  }
  const openModal2 = () => {
    setModal2(true)
  }

  return (
    <>
      {modal ? <ModalImage modal={modal} setModal={setModal} /> : null}
      {modal2 ? <ModalText modal={modal2} setModal={setModal2} /> : null}

      <Button onClick={openModal} className="button btn" type="button">
        Open first modal
      </Button>

      <Button onClick={openModal2} className="button2 btn" type="button">
        Open second modal
      </Button>
    </>
  )
}

export default App
